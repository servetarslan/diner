**Diner**

Projeyi diner-prod.zip içinden direkt alıp sunucuya yükleyerek kurabilirsiniz.

src klasörü ise projede herhangi bir geliştirme yapmak için kullanacağınız dev ortamı aşağıda komutlar var.

ingilizce klasoru sonradan eklendıgı için orasının maalesef geliştirme ortamı yok derlenen son halinden duznelemeler yapıldı.


## Install Project

```
$ npm install
```

or

```
$ yarn install
```



## Start Project

```
$ npm start
```

or 

```
$ yarn start
```



## Clean Dist

```
$ npm clean
```

or 

```
$ yarn clean
```