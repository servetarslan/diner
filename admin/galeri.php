<?php session_start(); ob_start();
include("../inc/config.php");
include("../inc/session.php");

Function baslangic(){
global $uye;
$id		  = intval($_GET["id"]);
$post_id  = intval($_GET["post_id"]);
$tur	  = strip_tags($_GET["tur"]);
$editpost = strip_tags($_GET["editpost"]);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Yönetim Paneli</title>
        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon2.html" /> 
        <!-- Link css-->
        <link rel="stylesheet" type="text/css" href="css/zice.style.css"/>
        <link rel="stylesheet" type="text/css" href="css/icon.css"/>
        <link rel="stylesheet" type="text/css" href="css/ui-custom.css"/>
        <link rel="stylesheet" type="text/css" href="css/timepicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/colorpicker/css/colorpicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/elfinder/css/elfinder.css" />
        <link rel="stylesheet" type="text/css" href="components/datatables/dataTables.css"  />
        <link rel="stylesheet" type="text/css" href="components/validationEngine/validationEngine.jquery.css" />
         
        <link rel="stylesheet" type="text/css" href="components/jscrollpane/jscrollpane.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/fancybox/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/tipsy/tipsy.css" media="all" />
        <link rel="stylesheet" type="text/css" href="components/editor/jquery.cleditor.css"  />
        <link rel="stylesheet" type="text/css" href="components/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="components/confirm/jquery.confirm.css" />
        <link rel="stylesheet" type="text/css" href="components/sourcerer/sourcerer.css"/>
        <link rel="stylesheet" type="text/css" href="components/fullcalendar/fullcalendar.css"/>
        <link rel="stylesheet" type="text/css" href="components/Jcrop/jquery.Jcrop.css"  />
   
        
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->
        
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="components/ui/jquery.autotab.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/checkboxes/iphone.check.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/datatables/ColVis.js"></script>
        <script type="text/javascript" src="components/scrolltop/scrolltopcontrol.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mousewheel.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mwheelIntent.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/spinner/ui.spinner.js"></script>
        <script type="text/javascript" src="components/tipsy/jquery.tipsy.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/confirm/jquery.confirm.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js" ></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js" ></script>
        <script type="text/javascript" src="components/vticker/jquery.vticker-min.js"></script>
        <script type="text/javascript" src="components/sourcerer/sourcerer.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/flot/flot.pie.min.js"></script>
        <script type="text/javascript" src="components/flot/flot.resize.min.js"></script>
        <script type="text/javascript" src="components/flot/graphtable.js"></script>

        <script type="text/javascript" src="components/uploadify/swfobject.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>        
        <script type="text/javascript" src="components/checkboxes/customInput.jquery.js"></script>
        <script type="text/javascript" src="components/effect/jquery-jrumble.js"></script>
        <script type="text/javascript" src="components/filestyle/jquery.filestyle.js" ></script>
        <script type="text/javascript" src="components/placeholder/jquery.placeholder.js" ></script>
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js" ></script>
        <script type="text/javascript" src="components/imgTransform/jquery.transform.js" ></script>
        <script type="text/javascript" src="components/webcam/webcam.js" ></script>
		<script type="text/javascript" src="components/rating_star/rating_star.js"></script>
		<script type="text/javascript" src="components/dualListBox/dualListBox.js"  ></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>    

         
        </head>        
        <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
                     


					 
        <!--//  header -->             
					   <?php include("header.php") ?>
					   
         <!--//  header --> 
		 
		 
		 
		 
			<div id="shadowhead"></div>
            <div id="hide_panel"> 
                  <a class="butAcc" rel="0" id="show_menu"></a>
                  <a class="butAcc" rel="1" id="hide_menu"></a>
                  <a class="butAcc" rel="0" id="show_menu_icon"></a>
                  <a class="butAcc" rel="1" id="hide_menu_icon"></a>
            </div>           
      
                  <div id="left_menu">
                      <!--//  menu starts -->             
					   <?php include("menu.php") ?>
					   
					<!--//  menu finish -->
              </div>

            
            <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>
                            <!--//  menu starts -->             
							
							<?php include("fastmenu.php") ?>
					   
							<!--//  menu finish -->
					</div>
                    <div class="clear"></div>
                    
                         <div class="onecolumn" >
                        <div class="header"><span><span class="ico gray window"></span>Görsel Ekle</span></div>
                        <div class="clear"></div>
                        <div class="content" >

						<? if($editpost == ''){?>
                        <form action="galeri.php?Git=ekle&post_id=<?=$post_id;?>" enctype="multipart/form-data" method="post">
	
                              <div class="section">
                                  <label> Video  <small>Vimeo Video Id</small></label>
                                  <div> <input type="text" name="video" class=" small" /><span class="f_help"> </span></div>
                             </div>
							 
							 <div class="section">
                                  <label> Sıra  <small>Sıralama</small></label>
                                  <div> <input type="text" name="sira"  class=" small" /><span class="f_help">Listeleme Sırası</span></div>
                             </div>

							 <div class="section ">
								<label> Görsel <small>Görsel Yükle</small></label>
								<div>
									<input type="file" name="resim" class="fileupload" />
								</div>
                            </div>

                              <div class="section last">
                                  <div>

								 <input type="submit" value="Kaydet" class="uibutton loading" title="Kaydet" rel="1" />
								  </div>

							 </div>
                          </form>
						  
						<?}?>
						<? if($editpost == 'Y'){
							$cek = mysql_fetch_array(mysql_query("select * from multimedia where id = '$id' and post_id = '$post_id'"));
						?>
                        <form action="galeri.php?Git=edit&post_id=<?=$post_id;?>&id=<?=$id?>" enctype="multipart/form-data" method="post">
	
                              <div class="section">
                                  <label> Video  <small>Vimeo Video Id</small></label>
                                  <div> <input type="text" name="video" value="<?=$cek["video"]?>" class=" small" /><span class="f_help"> </span></div>
                             </div>
							 
							 <div class="section">
                                  <label> Sıra  <small>Sıralama</small></label>
                                  <div> <input type="text" name="sira" value="<?=$cek["sira"]?>"  class=" small" /><span class="f_help">Listeleme Sırası</span></div>
                             </div>
							 
							<? if($cek["resim"] != ''){$image = '../statics/kare/'.$cek["resim"];}else{$image = 'images/no.jpg';} ?>
							 <div class="section ">
								<label>Var Olan Resim <small></small></label>
								<div>
									<img src="<?=$image; ?>" style="border:1px solid #777;">
								</div>
                            </div>
							
							 <div class="section ">
								<label> Görsel <small>Görsel Yükle</small></label>
								<div>
									<input type="file" name="resim" class="fileupload" />
								</div>
                            </div>

                              <div class="section last">
                                  <div>

								 <input type="submit" value="Kaydet" class="uibutton loading" title="Kaydet" rel="1" />
								  </div>

							 </div>
                          </form>
						  
						<?}?>
						
                        </div>
                        </div>  
					<? if($editpost != 'Y'){ ?>
                    <div class="onecolumn" >
                    <div class="header">
                    <span ><span class="ico  gray random"></span>Var Olan Görseller</span>
                    </div><!-- End header -->	
                    <div class=" clear"></div>
                    <div class="content"  >
					<div class="tableName">
					
					<table class="display data_table2" >
						<thead>
							<tr>
								
								<th width="60"><div class="th_wrapp">Resim</div></th>
								<th width="200"><div class="th_wrapp">Video ID</div></th>
								<th width="50"><div class="th_wrapp">Sıra</div></th>
								<th width="240"><div class="th_wrapp">-</div></th>
								
								<th width="70">İşlem</th>
								
							</tr>
						</thead>
						<tbody>
						
						<?php $basvuru= mysql_query("SELECT * FROM multimedia where post_id = '$post_id' order by id DESC"); 
										while($byaz = mysql_fetch_array($basvuru)){

								if($byaz["resim"] != ''){$image = '../statics/kare/'.$byaz["resim"];}else{$image = 'images/no.jpg';}
								?>
						
						
								<tr class="odd gradeX">
								
								<td><a href="galeri.php?id=<?php echo $byaz["id"]; ?>&post_id=<?=$post_id;?>&editpost=Y" ><img src="<?php echo $image;?>" width="50" style="border:1px solid #666;" ></a></td>
								<td><?php echo $byaz["video"]; ?></td>
								<td><?=$byaz["sira"]?></td>
								<td>-</td>
								<td>	
								  <span class="tip" >
									  <a href="galeri.php?id=<?php echo $byaz["id"]; ?>&post_id=<?=$post_id;?>&editpost=Y" title="Düzenle"  >
										  <img src="images/icon/icon_edit.png" width="18" >
									  </a>
								  </span> -
								  <span class="tip" >
									  <a href="galeri.php?Git=sil&id=<?php echo $byaz["id"]; ?>&post_id=<?=$post_id;?>" onclick="return confirm('Dikkat ! Varolanı silmek üzeresin ?');"   title="Sil"  >
										  <img src="images/icon/icon_delete.png" >
									  </a>
								  </span> 
								</td>
							</tr>
							
							<?php } ?>
							
						</tbody>
					</table>
                    </div>
                    
                    </div>
                    </div>
					
					<? }?>

						
					   
					   <?php include("fouter.php") ?>
					   
						<!--//  Fouter finish -->
                        
                                           
                    
					</div> <!--// End inner -->
            </div> <!--// End content --> 
</body>
</html>

<?php }

Function ekle(){
	
global $uye;
$id = intval($_GET["id"]);
$post_id = intval($_GET["post_id"]);

$sira = strip_tags(trim($_POST["sira"]));
$video = strip_tags(trim($_POST["video"]));

	if(@$_FILES["resim"]["type"] != ""){
		$resimadi= $video."-".rand(1,999);
		$foo = new Upload($_FILES['resim']);
		if ($foo->uploaded){
		echo "Yukleniyor...";
		$foo->file_new_name_body = "$resimadi";
		$foo->allowed = array ( 'image/*' );
		$foo->file_name_body_pre = 'kent-laboratuvar-';
		$foo->Process(DIR_RESIM);
		if ($foo->processed){
		$resim = $foo->file_dst_name;}}

		if ($foo->uploaded){
			$foo->file_new_name_body = "$resimadi";
			$foo->allowed = array ( 'image/*' );
			$foo->file_name_body_pre = 'kent-laboratuvar-';
			$foo->image_resize          = true;
			$foo->image_ratio_fill      = true;
			$foo->image_x               = 750;
			$foo->image_y               = 450;
			$foo->Process(DIR_LISTE);
			if ($foo->processed){
			$liste = $foo->file_dst_name;}}

		if ($foo->uploaded){
			$foo->file_new_name_body = "$resimadi";
			$foo->allowed = array ( 'image/*' );
			$foo->file_name_body_pre = 'kent-laboratuvar-';
			$foo->image_resize          = true;
			$foo->image_ratio_fill      = true;
			$foo->image_x               = 120;
			$foo->image_y               = 85;
			$foo->Process(DIR_KARE);
			if ($foo->processed){
			$kare = $foo->file_dst_name;}}

			$res1 = ',resim';
			$res2 = ",'$resim'";
			}else{
			$res1 = '';
			$res2 = '';
		}		
							
										
	
	$ekle = mysql_query("insert into multimedia (post_id,video,sira $res1) values ('$post_id','$video','$sira' $res2)");	
		if($ekle){
			header("Refresh: 0; url=galeri.php?post_id=$post_id");
		}
	}
	
Function edit(){
	
global $uye;
$id = intval($_GET["id"]);
$post_id = intval($_GET["post_id"]);

$sira = strip_tags(trim($_POST["sira"]));
$video = strip_tags(trim($_POST["video"]));

	if(@$_FILES["resim"]["type"] != ""){
		$resimadi= $video."-".rand(1,999);
		$foo = new Upload($_FILES['resim']);
		if ($foo->uploaded){
		echo "Yukleniyor...";
		$foo->file_new_name_body = "$resimadi";
		$foo->allowed = array ( 'image/*' );
		$foo->file_name_body_pre = 'kent-laboratuvar-';
		$foo->Process(DIR_RESIM);
		if ($foo->processed){
		$resim = $foo->file_dst_name;}}


	if ($foo->uploaded){
		$foo->file_new_name_body = "$resimadi";
		$foo->allowed = array ( 'image/*' );
		$foo->file_name_body_pre = 'kent-laboratuvar-';
		$foo->image_resize          = true;
		$foo->image_ratio_crop      = true;
		$foo->image_x               = 130;
		$foo->image_y               = 90;
		$foo->Process(DIR_KARE);
		if ($foo->processed){
		$kare = $foo->file_dst_name;}}

		$res = ",resim ='$resim'";

		}else{
			$res = '';
	}	
							
										
	
		$ekle = mysql_query("update multimedia set post_id = '$post_id', video = '$video',sira='$sira' $res where id = '$id'") or die (mysql_error());
		if($ekle){
			header("Refresh: 0; url=galeri.php?post_id=$post_id");
			 }
	  
}

Function sil(){
	
global $uye;
$id = intval($_GET["id"]);
$post_id = intval($_GET["post_id"]);
			
	
			$sil = mysql_query("delete from multimedia where id = '$id'");
			if($sil){
				header("Refresh: 0; url=galeri.php?post_id=$post_id");
			 }
	  
}
	  
	  


$Git = $_GET["Git"];
switch($Git){
	
	default:
	baslangic();
	break;

		
	 case "ekle":
     ekle();
	 break;	 		
	 
	 case "edit":
     edit();
	 break;
	 		
	 case "sil":
     sil();
	 break;
	 
}?>
