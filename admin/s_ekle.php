<?php /** Coder : Servet Arslan - Email : askardanadam@gmail.com - Phone : +90 544 719 02 50 **/
session_start(); ob_start();
include("../inc/config.php");
include("../inc/session.php");
include("../inc/fonk.php");

Function baslangic(){
global $uye;
$types = "S_liste";
$tur = $_GET['tur'];
$uploadtype = 'new';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />

        <title>Yönetim Paneli</title>
        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon2.html" />
        <!-- Link css-->
        <link rel="stylesheet" type="text/css" href="css/zice.style.css"/>
        <link rel="stylesheet" type="text/css" href="css/icon.css"/>
        <link rel="stylesheet" type="text/css" href="css/ui-custom.css"/>
        <link rel="stylesheet" type="text/css" href="css/timepicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/colorpicker/css/colorpicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/elfinder/css/elfinder.css" />
        <link rel="stylesheet" type="text/css" href="components/datatables/dataTables.css"  />
        <link rel="stylesheet" type="text/css" href="components/validationEngine/validationEngine.jquery.css" />

        <link rel="stylesheet" type="text/css" href="components/jscrollpane/jscrollpane.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/fancybox/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/tipsy/tipsy.css" media="all" />
        <link rel="stylesheet" type="text/css" href="components/editor/jquery.cleditor.css"  />
        <link rel="stylesheet" type="text/css" href="components/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="components/confirm/jquery.confirm.css" />
        <link rel="stylesheet" type="text/css" href="components/sourcerer/sourcerer.css"/>
        <link rel="stylesheet" type="text/css" href="components/fullcalendar/fullcalendar.css"/>
        <link rel="stylesheet" type="text/css" href="components/Jcrop/jquery.Jcrop.css"  />


        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->




        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.autotab.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/checkboxes/iphone.check.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/datatables/ColVis.js"></script>
        <script type="text/javascript" src="components/scrolltop/scrolltopcontrol.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mousewheel.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mwheelIntent.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/spinner/ui.spinner.js"></script>
        <script type="text/javascript" src="components/tipsy/jquery.tipsy.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/confirm/jquery.confirm.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js" ></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js" ></script>
        <script type="text/javascript" src="components/vticker/jquery.vticker-min.js"></script>
        <script type="text/javascript" src="components/sourcerer/sourcerer.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/flot/flot.pie.min.js"></script>
        <script type="text/javascript" src="components/flot/flot.resize.min.js"></script>
        <script type="text/javascript" src="components/flot/graphtable.js"></script>

        <script type="text/javascript" src="components/uploadify/swfobject.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>
        <script type="text/javascript" src="components/checkboxes/customInput.jquery.js"></script>
        <script type="text/javascript" src="components/effect/jquery-jrumble.js"></script>
        <script type="text/javascript" src="components/filestyle/jquery.filestyle.js" ></script>
        <script type="text/javascript" src="components/placeholder/jquery.placeholder.js" ></script>
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js" ></script>
        <script type="text/javascript" src="components/imgTransform/jquery.transform.js" ></script>
        <script type="text/javascript" src="components/webcam/webcam.js" ></script>
		<script type="text/javascript" src="components/rating_star/rating_star.js"></script>
		<script type="text/javascript" src="components/dualListBox/dualListBox.js"  ></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>

        <script type="text/javascript" src="js/zice.custom.js"></script>
		<script src="ckeditor/ckeditor.js" type="text/javascript"></script>
		
	
        </head>
        <body class="dashborad">
        <div id="alertMessage" class="error"></div>




        <!--//  header -->
					   <?php include("header.php") ?>

         <!--//  header -->




			<div id="shadowhead"></div>
            <div id="hide_panel">
                  <a class="butAcc" rel="0" id="show_menu"></a>
                  <a class="butAcc" rel="1" id="hide_menu"></a>
                  <a class="butAcc" rel="0" id="show_menu_icon"></a>
                  <a class="butAcc" rel="1" id="hide_menu_icon"></a>
            </div>

                  <div id="left_menu">
                      <!--//  menu starts -->
					   <?php include("menu.php") ?>

					<!--//  menu finish -->
              </div>


            <div id="content">
                <div class="inner">
					<div class="topcolumn">
                            <!--//  menu starts -->

							<?php include("fastmenu.php") ?>

							<!--//  menu finish -->
					</div>
                    <div class="clear"></div>

                        <div class="onecolumn" >
                        <div class="header"><span><span class="ico gray window"></span><?=$tur;?> Ekle</span></div>
                        <div class="clear"></div>
                        <div class="content" >


                        <form action="s_ekle.php?Git=ekle&tur=<?=$tur;?>" enctype="multipart/form-data" method="post" id="demo">

                              <div class="section">
                                  <label> Başlık  <small>Başlık Bölümü</small></label>
                                  <div> <input type="text" name="ad" required class=" large" /><span class="f_help"></span></div>
                             </div>
                             <? if($tur == 'page' || $tur == 'slider' || $tur == 'slider2' || $tur == 'blog'){?>
                              <div class="section">
                                  <label> Title  <small>SEO Başlık Bölümü</small></label>
                                  <div> <input type="text" name="title" class=" large" /><span class="f_help"> </span></div>
                             </div>
                              <div class="section">
                                  <label>URL Adresi <small>Yönlendirmek istediğiniz adres</small></label>
                                  <div> <input type="text" name="link"  class=" large" /><span class="f_help">Lütfen; <strong style="color:red;">ğ-ş-ü-ç-ö-@-/-?-*-^-%-'-!-£-$-(-)-=-,-:-;->-<-é-+-½-}-{ .html, .php</strong> vb. karakterler Kullanmayınız.</span></div>
                             </div>
                            <?php }?>

							  <? if($tur != 'blog'){ ?>
                             <div class="section">
                                  <label> Sıra  <small>Sıralama</small></label>
                                  <div> <input type="text" name="sira"  class="medium" /><span class="f_help">Listeleme Sırası</span></div>
                             </div>
							  <? } ?>
							 <? if($tur == 'components'){?>
	
                              <div class="section">
                                  <label> Uniq Kod  <small></small></label>
                                  <div> <input type="text" name="ajans" class=" medium" /><span class="f_help"></span></div>
                             </div>
                        
							 <? } ?>
							 
							 

							<?  if($tur == 'slider'){?>
                             <div class="section">
                                  <label> Buton Yazısı  <small></small></label>
                                  <div> <input type="text" name="kreatif" class=" medium" /><span class="f_help"></span></div>
                             </div>
							 <? } ?>

                   


							 <? if($tur == 'blog') {?>

							 <? if($tur!= 'Konu'){$kelime = 'Anasayfa';}else{$kelime='Menü';}?>
                             <div class="section">
                                  <label> <?=$kelime;?>  <small><?=$kelime;?> da yer alsın</small></label>
                                  <div> <input type="checkbox" class="on_off_checkbox" name="menu" value="1"  /><span class="f_help">Evet / Hayır</span></div>
                             </div>
							 <? } ?>
                             <? if($tur == 'blog' || $tur == 'slider' || $tur == 'slider2' || $tur == 'howdowork' || $tur == 'clients'){?>
							<div class="section ">
								<label> Resim <small>Resim Yükle</small></label>
								<div>
									<input type="file" name="resim"  class="fileupload" />
								</div>
                            </div>
                            
                             <? } ?>

							<? if($tur == 'sss' || $tur == 'sssHelp' || $tur == 'blog' || $tur == 'page' || $tur == 'components'){?>
							<div class="section">
								<label>İçerik  Açıklama<small>Detaylı Açıklaması</small></label>
								<div>
								<textarea cols="60" rows="10" name="aciklama" id="location2" class="ckeditor"></textarea>
								<script language="javascript" type="text/javascript">
                                CKEDITOR.replace('aciklama',{
                                    filebrowserBrowseUrl: '/doris/browser/browse.php',
                                    filebrowserImageBrowseUrl: '/doris/browser/browse.php?type=Images',
                                    filebrowserUploadUrl: '/doris/uploader/upload.php',
                                    filebrowserImageUploadUrl: '/doris/uploader/upload.php?type=Images',
                                    filebrowserWindowWidth: '900',
                                    filebrowserWindowHeight: '400',
                                    filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
                                    filebrowserImageBrowseUrl: 'ckfinder/ckfinder.html?Type=Images',
                                    filebrowserFlashBrowseUrl: 'ckfinder/ckfinder.html?Type=Flash',
                                    filebrowserUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                                    filebrowserImageUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                                    filebrowserFlashUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                                });
                            </script>
								<span class="f_help">Metinleri yapıştırmadan önce not defterine yapıştırıp daha sonra buraya yapıştırınız.</span></div>
                            </div>
							<? }?>
							<div class="section">
                                  <label>Description <small></small></label>
                                  <div> <textarea  name="ozet"   class=" large" ></textarea><span class="f_help"></span></div>
                             </div>



							   <div class="section">
                                  <label> Etiketler <small>SEO (Keyword)</small></label>
                                  <div> <input type="text" name="etiket" class=" large" /><span class="f_help">Yazı İçindeki Anahtar Kelimeler</span></div>
                             </div>
                              <div class="section last">
                                  <div>

								 <input type="submit" value="Kaydet" class="uibutton loading" title="Kaydet" rel="1" />
								  </div>

							 </div>
                          </form>
                        </div>
                        </div>

					   <?php include("fouter.php") ?>
					</div>
            </div>
			


</body>
</html>

<?php }

Function ekle(){
global $uye;
$uploadtype = 'new';

	
		include('upload_set.php');
	
	}



$Git = $_GET["Git"];
switch($Git){

	default:
	baslangic();
	break;

	 case "ekle":
     ekle();
	 break;

}?>
