const {
  src,
  dest,
  watch,
  series,
  parallel
} = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const useref = require('gulp-useref');
const uglify = require('gulp-uglify-es').default;
const gulpIf = require('gulp-if');
const cssnano = require('gulp-cssnano');
const imagemin = require('gulp-imagemin');
const plumber = require('gulp-plumber');
const cache = require('gulp-cache');
const del = require('del');
const wait = require('gulp-wait');
const babel = require("gulp-babel");
const concat = require("gulp-concat");

const rev = require('gulp-rev');
var revReplace = require('gulp-rev-replace');

const files = {
  scssPath: 'src/assets/sass/*.+(scss|sass)',
  scssWatchPath: 'src/assets/sass/**/*.+(scss|sass)',
  jsPath: 'src/assets/js/**/*.js',
  mainJs: 'src/assets/js/main.js',
  imgPath: 'src/assets/img/**/*.+(png|jpg|jpeg|gif|svg|ico)',
  fontPath: 'src/assets/fonts/**/*',
  htmlPath: 'src/*.php',
  sassDevPath: 'src/assets/css',
  jsDevPath: 'src/assets/js',
  cssDistPath: 'dist/assets/css',
  jsDistPath: 'dist/assets/js',
  imgDistPath: 'dist/assets/img',
  fontDistPath: 'dist/assets/fonts',
}

// -- DEVELOPMENT -- //
function sassTask() {
  return src(files.scssPath)
    .pipe(wait(500))
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(autoprefixer())
    .pipe(sass())
    .pipe(sourcemaps.write('./maps'))
    .pipe(dest(files.sassDevPath))
};


function jsTask() {
  return src(files.mainJs)
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(concat("app.js"))
    .pipe(sourcemaps.write("."))
    .pipe(dest(files.jsDevPath))
}


// BrowserSync
function serve(done) {
  browserSync.init({
    server: {
      baseDir: "./src/"
    },
    port: 5000
  });
  done();
}

// BrowserSync Reload
function reload(done) {
  browserSync.reload();
  done();
}


function watchTask() {
  watch([files.scssWatchPath, files.mainJs, files.htmlPath],
    series(
      parallel([sassTask]),
      reload
    )
  );
}


// -- DISTRIBUTION -- //

function userefTask() {
  return src(files.htmlPath)
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify().on('error', console.error)))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulpIf('*.js', rev()))
    .pipe(gulpIf('*.css', rev()))
    .pipe(revReplace({
      replaceInExtensions: ['.php']
    }))
    .pipe(dest('dist/'))
};

function imageminTask() {
  return src(files.imgPath)
    .pipe(cache(imagemin({
      interlaced: true
    })))
    .pipe(dest(files.imgDistPath))
}

function fonts() {
  return src(files.fontPath)
    .pipe(dest(files.fontDistPath))
}

function clean() {
  return del(["./dist"]);
}

function clone() {
  return src([
      './src/assets/video/**/*.mp4'
    ])
    .pipe(dest('dist/assets/video/'));
}

function cloneRobot() {
  return src([
      './src/Robot.txt', './src/.htaccess'
    ])
    .pipe(dest('dist/'));
}


exports.default = series(
  parallel(sassTask),
  serve,
  reload,
  watchTask
);

exports.img = imageminTask;
exports.clean = clean;
exports.fonts = fonts;
exports.build = series(clean, sassTask, parallel(imageminTask, fonts, clone, cloneRobot), userefTask)




// function pwaFiles(){
//   return src([
//     'src/manifest.json',
//     'src/service-worker.js',
//     'src/cache-service-worker.js',
//   ])
//   .pipe(dest('dist/'));
// }


// task('pwa', function (callback) {
//   runSequence('generate-service-worker', ['pwa-files'], callback);
// });

// // for pwa
// const workbox = require('workbox-build');
// const dist = './dist';

// task('generate-service-worker', () => {
//   return workbox.generateSW({
//     globDirectory: './dist',
//     globPatterns: [],
//     runtimeCaching: [{
//       urlPattern: /\.(?:png|jpg|jpeg|svg|woff2|woff|ttf|otf|eot|css|js)$/,
//       handler: 'CacheFirst',
//       options: {
//         cacheName: 'assets',
//         expiration: {
//           maxEntries: 75,
//         },
//       },
//     }],
//     swDest: `./src/cache-service-worker.js`,
//     clientsClaim: true,
//     skipWaiting: true,
//   }).then(({
//     warnings
//   }) => {
//     console.info('Service worker generation completed.', warnings);
//   }).catch((error) => {
//     console.warn('Service worker generation failed:', error);
//   });
// });