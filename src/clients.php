 <!-- Testimonial Section Start -->
 <section class="clients section-padding">
   <div class="container">
     <div class="section-header text-center">
       <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">
         <?php getComponent("clients_title", "aciklama");?>
      </h2>
       <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
     </div>
     <div class="row justify-content-center">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div id="clients" class="owl-carousel wow fadeInUp" data-wow-delay="0.4s">

         <?php 
            $gets = mysql_query("select * from kategori where tur = 'clients' and durum = '1' order by sira asc");
                while($works = mysql_fetch_array($gets)){
           ?>
           <div class="item">
             <img src="<?=HTTP_KUCUK.$works["resim"];?>" alt="">
           </div>
           <?php }?>
           
         </div>
       </div>
     </div>
   </div>
 </section>