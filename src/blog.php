<?php 
    include("inc.php"); 
    $link = strip_tags(trim(editle($_GET["link"])));
    $cek = mysql_fetch_array(mysql_query("select * from kategori where link = 'blog' and tur = 'page' and durum = '1'"));
?>
 <!DOCTYPE html>
 <html lang="en">

 <head>
   <!-- Required meta tags -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   <title><?=strip_tags($cek["title"]);?></title>
  <meta name="robots" content="index, follow">
  <meta name="description" content="<?=strip_tags($cek["ozet"]);?>" />
  <meta name="keywords" content="<?=strip_tags($cek["etiket"]);?>" />

  <?php include("meta.php")?>


   <!-- build:css assets/css/build.css -->
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="assets/css/bootstrap.min.css">
   <!-- Icon -->

   <!-- Owl carousel -->
   <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
   <link rel="stylesheet" href="assets/css/owl.theme.css">

   <!-- Animate -->
   <link rel="stylesheet" href="assets/css/animate.css">
   <!-- Main Style -->
   <link rel="stylesheet" href="assets/css/main.css">
   <!-- endbuild -->

 </head>

 <body>

   <?php include("header.php"); ?>

   <section id="blog" class="section-padding bg-gray ptop-100">
         <div class="container">
             <div class="section-header text-center">
                 <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">BLOG</h2>
                 <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
             </div>
             <div class="row">
             <?php 
                $gets = mysql_query("select * from kategori where tur = 'blog' and durum = '1' order by id desc");
                        while($works = mysql_fetch_array($gets)){
                ?>
                

                 <div class="col-lg-4 col-md-6 col-xs-12">
                     <div class="blog-item wow fadeInRight" data-wow-delay="0.2s">
                         <div class="blog-img">
                             <img class="img-fluid" src="<?=HTTP_KUCUK.$works["resim"];?>" alt="">
                         </div>
                         <div class="contetn">
                             <div class="info-text">
                                 <h3><a href="<?=$works["link"];?>"><?=$works["ad"];?></a></h3>
                                 <p><?=$works["tarih"];?></p>
                             </div>
                             <p><?=$works["ozet"];?></p>
                         </div>
                     </div>
                 </div>

                 <?php }?>



             </div>
         </div>
     </section>


  <?php include("footer.php"); ?>

  <!-- Footer Section Start -->


   <!-- build:js assets/js/bundle.js async -->
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="assets/js/jquery-min.js"></script>
   <script src="assets/js/popper.min.js"></script>
   <script src="assets/js/bootstrap.min.js"></script>
   <script src="assets/js/owl.carousel.min.js"></script>
   <script src="assets/js/wow.js"></script>
   <script src="assets/js/scrolling-nav.js"></script>
   <script src="assets/js/jquery.easing.min.js"></script>
   <script src="assets/js/app.js"></script>
   <script src="assets/js/form-validator.min.js"></script>
   <script src="assets/js/contact-form-script.min.js"></script>
   <!-- endbuild -->

 </body>

 </html>