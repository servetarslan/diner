<section class="section-padding how-do-work-2">
<div class="container">
<div class="section-header text-center">
<h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">
    <?php getComponent("help_howdowork", "aciklama");?>
</h2>

<div class="shape wow fadeInDown" data-wow-delay="0.3s">&nbsp;</div>
</div>

<div class="row wow fadeInUp" data-wow-delay="0.3s">
<div class="col-md-6"><img class="how-work-img" src="" /></div>

<div class="col-md-6">
<ul class="how-work-list">
    <?php 
        $gets = mysql_query("select * from kategori where tur = 'howdowork' and durum = '1' order by sira asc");
            while($works = mysql_fetch_array($gets)){
    ?>
        <li data-img="<?php echo HTTP_KUCUK.$works["resim"]; ?>"><strong><?=strip_tags($works["sira"]);?></strong> <span><?=strip_tags($works["ad"]);?></span></li>
    <?php } ?>
</ul>
</div>
</div>
</div>
</section>
