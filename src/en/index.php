<?php include("inc.php"); ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title><?=strip_tags($ceks["title"]);?></title>
  <meta name="robots" content="index, follow">
  <meta name="description" content="<?=strip_tags($ceks["description"]);?>" />
  <meta name="keywords" content="<?=strip_tags($ceks["keyword"]);?>" />

  <?php include("meta.php")?>


  <link rel="stylesheet" href="../assets/css/build-5f648754e8.css">

</head>

<body>

  <?php include("header.php"); ?>

    <!-- Hero Area Start -->
    <?php getComponent("herohome", "aciklama");?>
    <!-- Hero Area End -->

  <!-- Services Section Start -->
  
  <?php getComponent("home_3feature", "aciklama");?>
   
  
  <!-- Services Section End -->


  <!-- Features Section Start -->
  <?php getComponent("home_howdowork", "aciklama");?>
  <!-- Features Section End -->

  <?php include("clients.php");?>

  <!-- Call To Action Section Start -->
  <?php getComponent("app_store", "aciklama");?>
<!-- Call To Action Section Start -->


  <?php include("footer.php"); ?>

  <!-- Footer Section Start -->




  <script src="../assets/js/bundle-fac93013c1.js" async></script>

</body>

</html>