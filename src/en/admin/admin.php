<?php session_start(); ob_start();
include("../inc/config.php");
include("../inc/session.php");
Function baslangic(){
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Yönetim Paneli</title>
        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon2.html" /> 
        <!-- Link css-->
        <link rel="stylesheet" type="text/css" href="css/zice.style.css"/>
        <link rel="stylesheet" type="text/css" href="css/icon.css"/>
        <link rel="stylesheet" type="text/css" href="css/ui-custom.css"/>
        <link rel="stylesheet" type="text/css" href="css/timepicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/colorpicker/css/colorpicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/elfinder/css/elfinder.css" />
        <link rel="stylesheet" type="text/css" href="components/datatables/dataTables.css"  />
        <link rel="stylesheet" type="text/css" href="components/validationEngine/validationEngine.jquery.css" />
         
        <link rel="stylesheet" type="text/css" href="components/jscrollpane/jscrollpane.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/fancybox/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/tipsy/tipsy.css" media="all" />
        <link rel="stylesheet" type="text/css" href="components/editor/jquery.cleditor.css"  />
        <link rel="stylesheet" type="text/css" href="components/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="components/confirm/jquery.confirm.css" />
        <link rel="stylesheet" type="text/css" href="components/sourcerer/sourcerer.css"/>
        <link rel="stylesheet" type="text/css" href="components/fullcalendar/fullcalendar.css"/>
        <link rel="stylesheet" type="text/css" href="components/Jcrop/jquery.Jcrop.css"  />
   
        
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->
        
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="components/ui/jquery.autotab.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/checkboxes/iphone.check.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/scrolltop/scrolltopcontrol.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mousewheel.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mwheelIntent.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/spinner/ui.spinner.js"></script>
        <script type="text/javascript" src="components/tipsy/jquery.tipsy.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/confirm/jquery.confirm.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js" ></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js" ></script>
        <script type="text/javascript" src="components/vticker/jquery.vticker-min.js"></script>
        <script type="text/javascript" src="components/sourcerer/sourcerer.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/flot/flot.pie.min.js"></script>
        <script type="text/javascript" src="components/flot/flot.resize.min.js"></script>
        <script type="text/javascript" src="components/flot/graphtable.js"></script>

        <script type="text/javascript" src="components/uploadify/swfobject.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>        
        <script type="text/javascript" src="components/checkboxes/customInput.jquery.js"></script>
        <script type="text/javascript" src="components/effect/jquery-jrumble.js"></script>
        <script type="text/javascript" src="components/filestyle/jquery.filestyle.js" ></script>
        <script type="text/javascript" src="components/placeholder/jquery.placeholder.js" ></script>
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js" ></script>
        <script type="text/javascript" src="components/imgTransform/jquery.transform.js" ></script>
        <script type="text/javascript" src="components/webcam/webcam.js" ></script>
		<script type="text/javascript" src="components/rating_star/rating_star.js"></script>
		<script type="text/javascript" src="components/dualListBox/dualListBox.js"  ></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>        
           
        </head>        
        <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
         
		 
		 <!--//  header -->             
					   <?php include("header.php") ?>
					   
         <!--//  header -->
			
			
			<div id="shadowhead"></div>
            <div id="hide_panel"> 
                  <a class="butAcc" rel="0" id="show_menu"></a>
                  <a class="butAcc" rel="1" id="hide_menu"></a>
                  <a class="butAcc" rel="0" id="show_menu_icon"></a>
                  <a class="butAcc" rel="1" id="hide_menu_icon"></a>
            </div>           
                   
                  <div id="left_menu">
                    <!--//  menu starts -->             
					   <?php include("menu.php") ?>
					   
					<!--//  menu finish -->
					</div>
          
            
            <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>
                           


						   <!--//  menu starts -->             
							
							<?php include("fastmenu.php") ?>
					   
							<!--//  menu finish -->
					
					
					
					</div>
                    <div class="clear"></div>
                    
                    <div class="onecolumn" >
                        <div class="header"> <span ><span class="ico gray home"></span> Genel Admin Ayarları</span> </div>
                        <div class="clear"></div>
                        <div class="content" >
                            <div class="boxtitle min">Kullanıcı Adı Güncelle</div>
                              
							  <div  class="grid2">
                                <div class="inner">
                                    <form action="admin.php?Git=ekle" method="post">
                                        
								<?php $ayar = mysql_query("select * from admin"); $yayar = mysql_fetch_array($ayar); ?>		
										
										<div class="section">
                                            <label> Kullanıcı Adı </label>
                                            <div>
                                                <input type="text" name="ad" id="sitename"  class=" full"  value="<?php echo $yayar["kadi"] ?>" />
                                            </div>
                                        </div>
										
										
										
										<div class="section">
                                            <label> Parola </label>
                                            <div>
                                                <input type="password" name="parola" id="sitename"  class=" full" />
                                            </div>
                                        </div>
										
										
									  
									  
									  
                                      <div class="section"></div>
                                        <div class="section last">
                                            <div> 
											
											<input type="submit" class="uibutton loading"  title="Kaydet" value="Kaydet" rel="1" /> 
											<a href="index.php" type="reset" class="uibutton  confirm">Geri</a>
											
											
											
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>
							
							
							
							
							
							
							
							
							
							
							
							
                            <div class="clear"></div>
							
							
							
							
							
							
							
							<div class="boxtitle min">Parola Güncelle</div>
                              
							  
							  
							  <div  class="grid2">
                                <div class="inner">
                                    <form action="admin.php?Git=ekle2" method="post">
                                        
								<?php $ayar = mysql_query("select * from admin"); $yayar = mysql_fetch_array($ayar); ?>		
										
										<div class="section">
                                            <label> Eski Parola </label>
                                            <div>
                                                <input type="password" name="eski" id="sitename"  class=" full"  />
                                            </div>
                                        </div>
										
										
										
										<div class="section">
                                            <label>Yeni Parola </label>
                                            <div>
                                                <input type="password" name="yeni1" id="sitename"  class=" full" />
                                            </div>
                                        </div>
										
										<div class="section">
                                            <label>Yeni Parola(Tekrar) </label>
                                            <div>
                                                <input type="password" name="yeni2" id="sitename"  class=" full" />
                                            </div>
                                        </div>
										
									  
									  
									  
                                      <div class="section"></div>
                                        <div class="section last">
                                            <div> 
											
											<input type="submit" class="uibutton loading"  title="Kaydet" value="Kaydet" rel="1" /> 
											<a href="index.php" type="reset" class="uibutton  confirm">Geri</a>
											
											
											
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>
							
							</div>
							 <div class="clear"></div>
							
							
							
							
                        
						
						
						
						
                    </div>
                    <!-- // End onecolumn -->
                    
                    
                    <!-- // End onecolumn -->
                    
                    <!--// two column window -->
                       
                        
                        
                        
                        
					<!--//  Fouter starts -->             
					   
					   <?php include("fouter.php") ?>
					   
					<!--//  Fouter finish -->
                    
                   
                 

			   </div> <!--// End inner -->
            </div> <!--// End content --> 
</body>
</html>

<?php
}

Function ekle(){

$ad 					= $_POST["ad"];
$parola 				= md5($_POST["parola"]);

$sor = mysql_query("select * from admin"); $yazi= mysql_fetch_array($sor);
$sifre = $yazi["sifre"];

if($parola == md5($sifre)){

$ekle = mysql_query("update admin set kadi='$ad'");

if($ekle){ echo "<script>alert('Tebrikler Kayit Basarili!')</script>";
			header("Refresh: 0; url=admin.php");}

}else{echo "<script>alert('Yanlis Parola!')</script>";
			header("Refresh: 0; url=admin.php");}}
			
			
			
Function ekle2(){

$eski 					= md5($_POST["eski"]);
$yeni1 					= md5($_POST["yeni1"]);
$yeni2 					= md5($_POST["yeni2"]);

if($yeni1 != $yeni2){echo "<script>alert('Parolalar Eslesmiyor!')</script>";
			header("Refresh: 0; url=admin.php");}else{


$sor = mysql_query("select * from admin"); $yazi= mysql_fetch_array($sor);
$sifre = md5($yazi["sifre"]);

if($eski == $sifre){

$ekle = mysql_query("update admin set sifre='$yeni1'");

if($ekle){ echo "<script>alert('Tebrikler Kayit Basarili!')</script>";
			header("Refresh: 0; url=admin.php");}

}else{echo "<script>alert('Yanlis Parola!')</script>";
			header("Refresh: 0; url=admin.php");}}	}	

			
			
			
			
$Git = $_GET["Git"];
switch($Git){
	
	default:
	baslangic();
	break;
		
	 case "ekle":
     ekle();
	 break;
	 case "ekle2":
     ekle2();
	 break;
}


 ?>