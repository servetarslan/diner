<?php session_start(); ob_start();
include("../inc/config.php");
include("../inc/session.php");
Function baslangic(){
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />
        <meta name="keywords" content="" />
        
        <title>Yönetim Paneli</title>
        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon2.html" /> 
        <!-- Link css-->
        <link rel="stylesheet" type="text/css" href="css/zice.style.css"/>
        <link rel="stylesheet" type="text/css" href="css/icon.css"/>
        <link rel="stylesheet" type="text/css" href="css/ui-custom.css"/>
        <link rel="stylesheet" type="text/css" href="css/timepicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/colorpicker/css/colorpicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/elfinder/css/elfinder.css" />
        <link rel="stylesheet" type="text/css" href="components/datatables/dataTables.css"  />
        <link rel="stylesheet" type="text/css" href="components/validationEngine/validationEngine.jquery.css" />
         
        <link rel="stylesheet" type="text/css" href="components/jscrollpane/jscrollpane.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/fancybox/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/tipsy/tipsy.css" media="all" />
        <link rel="stylesheet" type="text/css" href="components/editor/jquery.cleditor.css"  />
        <link rel="stylesheet" type="text/css" href="components/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="components/confirm/jquery.confirm.css" />
        <link rel="stylesheet" type="text/css" href="components/sourcerer/sourcerer.css"/>
        <link rel="stylesheet" type="text/css" href="components/fullcalendar/fullcalendar.css"/>
        <link rel="stylesheet" type="text/css" href="components/Jcrop/jquery.Jcrop.css"  />
   
        
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->
        
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="components/ui/jquery.autotab.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/checkboxes/iphone.check.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/scrolltop/scrolltopcontrol.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mousewheel.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mwheelIntent.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/spinner/ui.spinner.js"></script>
        <script type="text/javascript" src="components/tipsy/jquery.tipsy.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/confirm/jquery.confirm.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js" ></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js" ></script>
        <script type="text/javascript" src="components/vticker/jquery.vticker-min.js"></script>
        <script type="text/javascript" src="components/sourcerer/sourcerer.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/flot/flot.pie.min.js"></script>
        <script type="text/javascript" src="components/flot/flot.resize.min.js"></script>
        <script type="text/javascript" src="components/flot/graphtable.js"></script>

        <script type="text/javascript" src="components/uploadify/swfobject.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>        
        <script type="text/javascript" src="components/checkboxes/customInput.jquery.js"></script>
        <script type="text/javascript" src="components/effect/jquery-jrumble.js"></script>
        <script type="text/javascript" src="components/filestyle/jquery.filestyle.js" ></script>
        <script type="text/javascript" src="components/placeholder/jquery.placeholder.js" ></script>
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js" ></script>
        <script type="text/javascript" src="components/imgTransform/jquery.transform.js" ></script>
        <script type="text/javascript" src="components/webcam/webcam.js" ></script>
		<script type="text/javascript" src="components/rating_star/rating_star.js"></script>
		<script type="text/javascript" src="components/dualListBox/dualListBox.js"  ></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>        
           
        </head>        
        <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 
         
		 
		 <!--//  header -->             
					   <?php include("header.php") ?>
					   
         <!--//  header -->
			
			
			<div id="shadowhead"></div>
            <div id="hide_panel"> 
                  <a class="butAcc" rel="0" id="show_menu"></a>
                  <a class="butAcc" rel="1" id="hide_menu"></a>
                  <a class="butAcc" rel="0" id="show_menu_icon"></a>
                  <a class="butAcc" rel="1" id="hide_menu_icon"></a>
            </div>           
                   
                  <div id="left_menu">
                    <!--//  menu starts -->             
					   <?php include("menu.php") ?>
					   
					<!--//  menu finish -->
					</div>
          
            
            <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>
                           


						   <!--//  menu starts -->             
							
							<?php include("fastmenu.php") ?>
					   
							<!--//  menu finish -->
					
					
					
					</div>
                    <div class="clear"></div>
                    
                    <div class="onecolumn" >
                        <div class="header"> <span ><span class="ico gray home"></span> Genel Panel Ayarları</span> </div>
                        <div class="clear"></div>
                        <div class="content" >
                            <div class="boxtitle min">Ekleme Yaptıktan Sonra İzleyeceği Yol</div>
                              <div  class="grid2">
                                <div class="inner">
                                    <form action="panel.php?Git=ekle" method="post">
                                        
								<?php $ayar = mysql_query("select * from ayar"); $yayar = mysql_fetch_array($ayar); ?>		
										
										
								<div class="section">
								<label>Haberler<small>Haber Ekledikten Sonra</small></label>   
								<div>
                              	  <select data-placeholder="Kategori Seçiniz" name="haber" class="chzn-select" tabindex="2" >
                                  <option value="haberler.php">Haberlere Geri Gelsin</option>
								  <option value="habereng.php?tr_id=$id">İngilizce Devam Etsin</option> 
								  <option value="haberekle.php">Yeni Haber Ekleme Bölümü</option> 
								  
                                </select>       
								</div>
								</div>
								
								
								<div class="section">
								<label>Video<small>Video Ekledikten Sonra</small></label>   
								<div>
                              	  <select data-placeholder="Kategori Seçiniz" name="video" class="chzn-select" tabindex="2" >
                                  <option value="video.php">Videoalara Geri Gelsin</option>
								  <option value="videoeng.php?tr_id=$id">İngilizce Devam Etsin</option> 
								  <option value="videoekle.php">Yeni Video Ekleme Bölümü</option> 
								  
                                </select>       
								</div>
								</div>
								
								
								
								<div class="section">
								<label>Slider<small>Slider Ekledikten Sonra</small></label>   
								<div>
                              	  <select data-placeholder="Kategori Seçiniz" name="slider" class="chzn-select" tabindex="2" >
                                  <option value="slider.php">Sliderlere Geri Gelsin</option>
								  <option value="slidereng.php?tr_id=$id">İngilizce Devam Etsin</option> 
								  <option value="sliderekle.php">Yeni Slider Ekleme Bölümü</option> 
								  
                                </select>       
								</div>
								</div>
								
								
								
								<div class="section">
								<label>Katalog<small>Katalog Ekledikten Sonra</small></label>   
								<div>
                              	  <select data-placeholder="Kategori Seçiniz" name="katalog" class="chzn-select" tabindex="2" >
                                  <option value="katalog.php">Kataloglara Geri Gelsin</option>
								  <option value="katalogeng.php?tr_id=$id">İngilizce Devam Etsin</option> 
								  <option value="katalogekle.php">Yeni Katalog Ekleme Bölümü</option> 
								  
                                </select>       
								</div>
								</div>

								   
									  
							
									  
									  
									  
									  
									  
									  
									  
                                      <div class="section"></div>
                                        <div class="section last">
                                            <div> 
											
											<input type="submit" class="uibutton loading"  title="Kaydet" value="Kaydet" rel="1" /> 
											<input type="reset" class="uibutton  confirm" value="Temizle" />
											
											
											
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>
							
							
							
							
							
                            <div class="clear"></div>
                        </div>
                    </div>
                    <!-- // End onecolumn -->
                    
                    
                    <!-- // End onecolumn -->
                    
                    <!--// two column window -->
                       
                        
                        
                        
                        
					<!--//  Fouter starts -->             
					   
					   <?php include("fouter.php") ?>
					   
					<!--//  Fouter finish -->
                    
                   
                 

			   </div> <!--// End inner -->
            </div> <!--// End content --> 
</body>
</html>

<?php
}

Function ekle(){

$ad 				= $_POST["ad"];
$title 				= $_POST["title"];
$keyword 			= $_POST["keyword"];
$description 		= $_POST["description"];
$fouter 			= $_POST["fouter"];
$analistic 			= $_POST["analistic"];
$mail 				= $_POST["mail"];
$maps 				= $_POST["maps"];
$tel 				= $_POST["tel"];
$admin 				= $_POST["admin"];
$statu 				= $_POST["online"];

$ekle = mysql_query("update ayar set ad='$ad', title='$title', keyword='$keyword', description='$description', fouter='$fouter', maps='$maps', analistic='$analistic', mail='$mail', tel='$tel', admin='$admin', durum='$statu' ");

if($ekle){ header("Location:index.php");}

}

$Git = $_GET["Git"];
switch($Git){
	
	default:
	baslangic();
	break;
		
	 case "ekle":
     ekle();
	 break;
	 
}


 ?>