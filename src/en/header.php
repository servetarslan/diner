<!-- Header Area wrapper Starts -->
<header id="header-wrap">
    <!-- Navbar Start -->
    <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <a href="index" title="<?=strip_tags($ceks["title"]);?>" class="navbar-brand"><img src="<?=HTTP_RESIM.strip_tags($ceks["resim"]);?>" title="<?=strip_tags($ceks["title"]);?>" alt="<?=strip_tags($ceks["description"]);?>"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="lni-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <?php getComponent("menu", "aciklama");?>
            </div>
        </div>
    </nav>
    <!-- Navbar End -->
</header>
<!-- Header Area wrapper End -->