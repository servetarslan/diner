<footer id="footer" class="footer-area section-padding">
     <div class="container">
       <div class="container">
         <div class="row">
           <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
             <div class="widget">
               <h3 class="footer-logo"><img src="<?=HTTP_RESIM.strip_tags($ceks["resim"]);?>" title="<?=strip_tags($ceks["title"]);?>" alt="<?=strip_tags($ceks["description"]);?>"></h3>
               <div class="textwidget">
                 <p><?=strip_tags($ceks["about"]);?></p>
               </div>

             </div>
           </div>
           <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
             <?php getComponent("footer_url", "aciklama");?>
           </div>
           <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
             <h3 class="footer-titel">Sign up</h3>
             <div class="textwidget">
               <p>Subscribe to our newsletter for latest news about DINER.</p>
             </div>
             <div class="news-form">
               <div class="form-group mtop-15">
                 <input type="email" class="form-control" placeholder="E-posta">
               </div>
               <div class="form-group">
                 <button class="btn btn-common">Sign up</button>
               </div>
             </div>
           </div>
           <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
             <h3 class="footer-titel">Contact us</h3>
             <ul class="address">
               <!-- <li>
                 <a href="#"><i class="lni-map-marker"></i> 105 Madison Avenue - <br> Third Floor New York, NY 10016</a>
               </li> -->
               <li>
                 <a href="#"><i class="lni-phone-handset"></i> P: <?=strip_tags($ceks["tel"]);?></a>
               </li>
               <li>
                 <a href="#"><i class="lni-envelope"></i> E: <?=strip_tags($ceks["mail"]);?></a>
               </li>
             </ul>
             <div class="social-icon">
               <a class="facebook" href="https://www.facebook.com/<?=strip_tags($ceks["facebook"]);?>"><i class="lni-facebook-filled"></i></a>
               <a class="twitter" href="https://twitter.com/<?=strip_tags($ceks["twitter"]);?>"><i class="lni-twitter-filled"></i></a>
               <a class="instagram" href="https://instagram.com/<?=strip_tags($ceks["instagram"]);?>"><i class="lni-instagram-filled"></i></a>
               <a class="linkedin" href="https://www.linkedin.com/<?=strip_tags($ceks["linkedin"]);?>"><i
                   class="lni-linkedin-filled"></i></a>
             </div>
           </div>
         </div>
       </div>
     </div>
     <div id="copyright">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="copyright-content">
               <p>2020 ARt Labs All Right Reserved</p>
             </div>
           </div>
         </div>
       </div>
     </div>
   </footer>
   <!-- Footer Section End -->

   <!-- Go to Top Link -->
   <a href="#" class="back-to-top">
     <i class="lni-arrow-up"></i>
   </a>

   <!-- Preloader -->
   <div id="preloader">
     <div class="loader" id="loader-1"></div>
   </div>
   <!-- End Preloader -->