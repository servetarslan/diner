<?php include("inc.php"); ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title><?=strip_tags($ceks["title"]);?></title>
  <meta name="robots" content="index, follow">
  <meta name="description" content="<?=strip_tags($ceks["description"]);?>" />
  <meta name="keywords" content="<?=strip_tags($ceks["keyword"]);?>" />

  <?php include("meta.php")?>


  <!-- build:css assets/css/build.css -->
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <!-- Icon -->

  <!-- Owl carousel -->
  <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="assets/css/owl.theme.css">

  <!-- Animate -->
  <link rel="stylesheet" href="assets/css/animate.css">
  <!-- Main Style -->
  <link rel="stylesheet" href="assets/css/main.css">
  <!-- endbuild -->

</head>

<body>

  <?php include("header.php"); ?>

    <!-- Hero Area Start -->
    <?php getComponent("herohome", "aciklama");?>
    <!-- Hero Area End -->

  <!-- Services Section Start -->
  
  <?php getComponent("home_3feature", "aciklama");?>
   
  
  <!-- Services Section End -->


  <!-- Features Section Start -->
  <?php getComponent("home_howdowork", "aciklama");?>
  <!-- Features Section End -->

  <?php include("clients.php");?>

  <!-- Call To Action Section Start -->
  <?php getComponent("app_store", "aciklama");?>
<!-- Call To Action Section Start -->


  <?php include("footer.php"); ?>

  <!-- Footer Section Start -->




  <!-- build:js assets/js/bundle.js async -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="assets/js/jquery-min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/owl.carousel.min.js"></script>
  <script src="assets/js/wow.js"></script>
  <script src="assets/js/scrolling-nav.js"></script>
  <script src="assets/js/jquery.easing.min.js"></script>
  <script src="assets/js/app.js"></script>
  <script src="assets/js/form-validator.min.js"></script>
  <script src="assets/js/contact-form-script.min.js"></script>
  <!-- endbuild -->

</body>

</html>