(function ($) {

  "use strict";

  $(window).on('load', function () {

    /*Page Loader active
    ========================================================*/
    $('#preloader').fadeOut();

    // Sticky Nav
    $(window).on('scroll', function () {
      if ($(window).scrollTop() > 50) {
        $('.scrolling-navbar').addClass('top-nav-collapse');
      } else {
        $('.scrolling-navbar').removeClass('top-nav-collapse');
      }
    });


    /* Auto Close Responsive Navbar on Click
    ========================================================*/
    function close_toggle() {
      if ($(window).width() <= 768) {
        $('.navbar-collapse a').on('click', function () {
          $('.navbar-collapse').collapse('hide');
        });
      } else {
        $('.navbar .navbar-inverse a').off('click');
      }
    }
    close_toggle();
    $(window).resize(close_toggle);

    /* WOW Scroll Spy
    ========================================================*/
    var wow = new WOW({
      //disabled for mobile
      mobile: false
    });

    wow.init();

    /* clients carousel 
    ========================================================*/
    var owl = $("#clients");
    owl.owlCarousel({
      loop: true,
      nav: false,
      dots: true,
      margin: 20,
      slideSpeed: 1000,
      stopOnHover: true,
      autoPlay: true,
      responsiveClass: true,
      responsiveRefreshRate: true,
      responsive: {
        0: {
          items: 2
        },
        768: {
          items: 3
        },
        960: {
          items: 4
        },
        1200: {
          items: 6
        },
        1920: {
          items: 7
        }
      }
    });


    /* clients carousel 
    ========================================================*/
    var owl = $("#restorant");
    owl.owlCarousel({
      loop: true,
      nav: false,
      dots: true,
      margin: 10,
      slideSpeed: 1000,
      autoPlay: true,
      responsiveClass: true,
      responsiveRefreshRate: true,
      responsive: {
        0: {
          items: 1
        },

        1200: {
          items: 1
        }

      }
    });

    $(".show-links").click(function (e) {
      var url = $(this).attr("href");
      $('html,body').animate({
        scrollTop: $(url).offset().top
      }, 700);

      e.preventDefault();
    });

    function howdowork() {

      let item = $(".how-work-list li");


      function imgs (){
        if ($(window).width() > 768) {
          item.hover(function () {
            $(".how-work-img").attr("src", $(this).data("img"));
            $(".how-work-list li").removeClass("active");
            $(this).addClass("active");
          })
          var event = new MouseEvent('mouseover', {
            'view': window,
            'bubbles': true,
            'cancelable': true
          });
          
          document.querySelectorAll(".how-work-list li")[0].dispatchEvent(event);
        } else {
          for (let is of document.querySelectorAll(".how-work-list li")) {
            let img = document.createElement("img");
            img.setAttribute("src", is.getAttribute("data-img"));
            is.insertBefore(img, is.firstChild);
          }
        }
      }

      if(item.length){
        imgs();
      }

      var vid = document.querySelector(".video-mockup-wrapper video");
      if(vid && $(window).width() < 768){
        document.querySelector(".phone-mockup-video").addEventListener("click", function(){
          if (vid.paused) {
            vid.play();
            } else {
              vid.pause();
          }
        })
      }
      
      

    }

    howdowork();


    /* Back Top Link active
    ========================================================*/
    var offset = 200;
    var duration = 500;
    $(window).scroll(function () {
      if ($(this).scrollTop() > offset) {
        $('.back-to-top').fadeIn(400);
      } else {
        $('.back-to-top').fadeOut(400);
      }
    });

    $('.back-to-top').on('click', function (event) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: 0
      }, 600);
      return false;
    });

  });


}(jQuery));