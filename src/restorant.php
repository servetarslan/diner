<?php 
    include("inc.php"); 
    $link = strip_tags(trim(editle($_GET["link"])));
    $cek = mysql_fetch_array(mysql_query("select * from kategori where link = 'for-businesses' and tur = 'page' and durum = '1'"));
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title><?=strip_tags($cek["title"]);?></title>
  <meta name="robots" content="index, follow">
  <meta name="description" content="<?=strip_tags($cek["ozet"]);?>" />
  <meta name="keywords" content="<?=strip_tags($cek["etiket"]);?>" />

  <?php include("meta.php")?>


  <!-- build:css assets/css/build.css -->
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <!-- Icon -->

  <!-- Owl carousel -->
  <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="assets/css/owl.theme.css">

  <!-- Animate -->
  <link rel="stylesheet" href="assets/css/animate.css">
  <!-- Main Style -->
  <link rel="stylesheet" href="assets/css/main.css">
  <!-- endbuild -->

</head>

<body>

  <!-- Header Area wrapper Starts -->
  <?php include("header-bg.php"); ?>

  <!-- Header Area wrapper End -->
  <!-- Hero Area Start -->
  <div class="restorant-slider">
    <div class="container">
      <h2 class="text-center wow fadeInUp" data-wow-delay="0.4s">
        <?php getComponent("restorant_head1", "aciklama");?>
      </h2>
      <div id="restorant" class="owl-carousel wow fadeInUp" data-wow-delay="0.6s">
        <?php 
            $gets = mysql_query("select * from kategori where tur = 'slider2' and durum = '1' order by sira asc");
                while($works = mysql_fetch_array($gets)){
           ?>
        <div class="item">
          <img src="<?=HTTP_RESIM.$works["resim"];?>" alt="">
        </div>
        <?php }?>

      </div>
      <?php getComponent("restorant_desc_button", "aciklama");?>


    </div>
  </div>
  <!-- Hero Area End -->
  <!-- Services Section Start -->
  
    <?php getComponent("restorant_6feature", "aciklama");?>

  
  <!-- Services Section End -->


  <!-- Features Section Start -->
  
    <?php getComponent("restorant_howdowork", "aciklama");?>


  <?php include("sss.php"); ?>

  
  <?php getComponent("price", "aciklama");?>

  <!-- Contact Section Start -->
  <section id="register" class="section-padding bg-gray">
    <div class="container">
      <div class="section-header text-center">
      <?php getComponent("form_head", "aciklama");?>

      </div>
      <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.3s">
        <div class="col-md-12">
          <div class="contact-block">
            <form id="contactForm">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" class="form-control" id="shopname" name="shopname" placeholder="İşletme Adı"
                      data-error="Lütfn bir işletme adı giriniz.">
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Ad Soyad (*)" required
                      data-error="Lütfen ad soyad giriniz">
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <select class="form-control" id="shopcategory">
                      <option>İşletme Kategorisi Seçiniz</option>
                      <?php 
                        $gets = mysql_query("select * from kategori where tur = 'cats' and durum = '1' order by sira asc");
                            while($works = mysql_fetch_array($gets)){
                      ?>
                     
                        <option value="<?php echo $works["link"];?>"><?php echo $works["ad"];?></option>
                      <?php }?>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" placeholder="E-posta" id="email" class="form-control" name="email"
                      data-error="Please enter your email">
                    <div class="help-block with-errors"></div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="tel" placeholder="Telefon (*)" id="phone" class="form-control" required name="phone"
                      data-error="Lütfen bir telefon numarası giriniz">
                    <div class="help-block with-errors"></div>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <textarea class="form-control" id="address" placeholder="Mesajınız" rows="7"
                      data-error="Mesaj yazınız."></textarea>
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
               
                <div class="col-md-12">
                  <div class="submit-button text-center">
                    <button class="btn btn-common" id="form-submit" type="submit"><?php getComponent("form_button", "aciklama");?></button>
                    <div id="msgSubmit" class="h3 text-center hidden"></div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- Contact Section End -->


  <!-- Testimonial Section Start -->
  <?php include("clients.php");?>
  <!-- Call To Action Section Start -->
  <!-- Call To Action Section Start -->
    <?php getComponent("app_store", "aciklama");?>
  <!-- Call To Action Section Start -->


  <?php include("footer.php"); ?>

  <!-- Footer Section Start -->


  <!-- build:js assets/js/bundle.js async -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="assets/js/jquery-min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/owl.carousel.min.js"></script>
  <script src="assets/js/wow.js"></script>
  <script src="assets/js/scrolling-nav.js"></script>
  <script src="assets/js/jquery.easing.min.js"></script>
  <script src="assets/js/app.js"></script>
  <script src="assets/js/form-validator.min.js"></script>
  <script src="assets/js/contact-form-script.min.js"></script>
  <!-- endbuild -->

</body>

</html>