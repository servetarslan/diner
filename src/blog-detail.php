<section id="blog" class="section-padding bg-gray ptop-100">
    <div class="container">
        <div class="wow fadeInRight" data-wow-delay="0.2s">
            <?php if($cek["resim"] != ""){?>
            <div class="text-center">
                <img class="img-fluid" src="<?=HTTP_RESIM.$cek["resim"];?>" alt="">
            </div>
            <? } ?>
            <div class="contetn">
                <div class="info-text">
                    <p>&nbsp;</p>
                    <h3><?=$cek["ad"];?></h3>
                    <p><?=$cek["tarih"];?></p>
                </div>
                <p><?=$cek["aciklama"];?></p>
            </div>
        </div>
    </div>
</section>