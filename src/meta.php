<link rel="canonical" href="<?=strip_tags($ceks["admin"]);?>" />

<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

<meta property="og:url" content="/" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?=strip_tags($ceks["title"]);?>" />
<meta property="og:description" content="<?=strip_tags($ceks["description"]);?>" />
<meta property="og:image" content="assets/img/favicon.png" />


<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', '<?=strip_tags($ceks["analistic"]);?>');
</script>